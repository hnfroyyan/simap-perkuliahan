<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\Dosen;
use App\Models\Mahasiswa;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Admin::create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('12345678')
        ]);

        $mahasiswa = Mahasiswa::create([
            'name' => 'mahasiswa',
            'email' => 'mahasiswa@mahasiswa.com',
            'password' => bcrypt('12345678')
        ]);

        $dosen = Dosen::create([
            'name' => 'dosen',
            'email' => 'dosen@dosen.com',
            'password' => bcrypt('12345678')
        ]);
    }
}
