<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendaftaransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendaftarans', function (Blueprint $table) {
            $table->id();
            $table->string('no_registrasi')->unique();
            $table->string('nama');
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->string('jenis_kelamin');
            $table->string('status_menikah');
            $table->string('kewarganegaraan');
            $table->string('agama');
            $table->text('alamat');
            $table->string('no_hp');
            $table->string('email')->unique();
            $table->string('no_telepon');
            $table->string('asal_sekolah');
            $table->string('jurusan')->nullable();
            $table->string('nama_asal_sekolah');
            $table->year('tahun_lulus');
            $table->string('no_ijazah')->unique();
            $table->text('alamat_sekolah');
            $table->string('nama_orangtua');
            $table->string('no_hp_orangtua');
            $table->string('email_orangtua')->unique()->nullable();
            $table->string('pekerjaan_orangtua');
            $table->string('nama_intansi_orangtua')->nullable();
            $table->string('pendidikan_terakhir_orangtua')->nullable();
            $table->string('kelas');
            $table->string('foto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendaftarans');
    }
}
