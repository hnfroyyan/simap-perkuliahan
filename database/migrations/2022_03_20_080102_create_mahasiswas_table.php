<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMahasiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswas', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('nim');
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->string('jenis_kelamin');
            $table->string('status_menikah');
            $table->string('kewarganegaraan');
            $table->string('agama');
            $table->string('alamat');
            $table->string('no_hp');
            $table->string('no_telepon');
            $table->string('asal_sekolah');
            $table->string('jurusan')->nullable();
            $table->string('nama_asal_sekolah');
            $table->year('tahun_lulus');
            $table->string('no_ijazah')->unique();
            $table->string('alamat_sekolah');
            $table->string('nama_orangtua');
            $table->string('no_hp_orangtua');
            $table->string('email_orangtua')->unique()->nullable();
            $table->string('pekerjaan_orangtua');
            $table->string('nama_intansi_orangtua')->nullable();
            $table->string('pendidikan_terakhir_orangtua')->nullable();
            $table->string('foto');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswas');
    }
}
